<?php

namespace Database\Seeders;

use Hash;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Juan Perez',
            'email' => 'juan@perez.com',
            'password' => Hash::make('password')
        ]);

        DB::table('oauth_clients')->insert([
            'id'                     => 1,
            'name'                   => 'Personal Access Client',
            'secret'                 => config('services.passport.secret'),
            'redirect'               => 'http://localhost',
            'personal_access_client' => 1,
            'password_client'        => 0,
            'revoked'                => 0
        ]);
    }
}
