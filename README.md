# Agree Challenge

## Pokemon Api Rest

### Descripción
Para crear la solución del Challenge, se creó un proyecto de Laravel 8, se protegieron las rutas del CRUD de pokemon con Laravel Passport. Se utilizaron ApiResources para formatear la data de la api y Form Request para validar la información recibida.

En cuanto a los campos requeridos, se tomó que los campos expansión y tipo son tipo string (aunque que se podría hacerlo como relación OneToMany) y el campo Rareza es una lista definida.

En cuanto a la api se crearon tanto para registrar y logear un usuario, como para crear, editar, borrar, guardar y listar pokemons (estas ultimas protegidas con token)

La lista de pokemons está paginada con 15 registros por página.

Se agregó a la raíz del proyecto un archivo de importación del software **Inmsonia Rest Api**, con todos los endpoint (local y producción) 

---

### Deploy
El proyecto está deployado en el servicio de Laravel Forge y utiliza una instancia de vps de AWS.
La url al proyecto es https://agree.diegonelson.com

---

### Swagger
La documentación de la api esta en https://agree.diegonelson.com/api/documentation

Para autenticar en la api, hay que hacer login con el usuario creado y copiar el valor del token. Luego clickear en el botón authorize y completar el input con la palabra Bearer seguido de un espacio y el valor del token copiado

`Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOi...`



