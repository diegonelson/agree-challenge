<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PokemonUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required|string|max:250',
            'hp'             => 'required|integer|multiple_of:10',
            'isFirstEdition' => 'required|boolean',
            'expansion'      => 'required|string|max:250',
            'type'           => 'required|string|max:250',
            'rarity'         => 'required|in:Comun,No Comun,Rara',
            'price'          => 'required|integer',
            'imageUrl'       => 'required|string|max:250'

        ];
    }
}
