<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PokemonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name'           => $this->name,
            'hp'             => $this->hp,
            'isFirstEdition' => $this->isFirstEdition,
            'expansion'      => $this->expansion,
            'type'           => $this->type,
            'rarity'         => $this->rarity,
            'price'          => $this->price,
            'imageUrl'       => $this->imageUrl,
            'cardCreatedAt'  => $this->created_at->format('Y-m-d'),
            'cardUrl'        => route('pokemon.show', $this->id)
        ];
    }
}
