<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PokemonStoreRequest;
use App\Http\Requests\PokemonUpdateRequest;
use App\Http\Resources\PokemonResource;
use App\Models\Pokemon;
use Illuminate\Http\Request;

class PokemonController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/pokemon",
     *      operationId="getPokemonList",
     *      tags={"Pokemon"},
     *      security={
     *          {"passport": {}},
     *      },
     *      summary="Get list of pokemons",
     *      description="Returns list of pokemons",
     *      @OA\Parameter(
     *         name="expansion",
     *         in="query",
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="type",
     *         in="query",
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="rarity",
     *         in="query",
     *         description= "Only Accept: Comun, No Comun or Rara",
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *           response=400,
     *           description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="not found"
     *      ),
     *  )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $expansion = $request->expansion;
        $type      = $request->type;
        $rarity    = $request->rarity;

        $pokemons = Pokemon::orderBy('name')
            ->when($expansion, function ($query, $expansion) {
                $query->where('expansion', 'LIKE', '%' . $expansion . '%');
            })
            ->when($type, function ($query, $type) {
                $query->where('type', 'LIKE', '%' . $type . '%');
            })
            ->when($rarity, function ($query, $rarity) {
                $query->where('rarity', $rarity);
            })
            ->paginate();

        return PokemonResource::collection($pokemons);
    }

    /**
     * @OA\Post(
     *      path="/api/pokemon",
     *      operationId="postPokemonStore",
     *      tags={"Pokemon"},
     *      security={
     *          {"passport": {}},
     *      },
     *      summary="Store a Pokemon",
     *      description="Store a Pokemon",
     *      @OA\Parameter(
     *         name="name",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="hp",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="isFirstEdition",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="boolean"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="expansion",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="type",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="rarity",
     *         in="query",
     *         required=true,
     *         description= "Only Accept: Comun, No Comun or Rara",
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="price",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="imageUrl",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *          
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *           response=400,
     *           description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="not found"
     *      ),
     *  )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PokemonStoreRequest $request)
    {
        $pokemon = Pokemon::create($request->validated());

        return new PokemonResource($pokemon);
    }

    /**
     * @OA\Get(
     *      path="/api/pokemon/{id}",
     *      operationId="postPokemonShow",
     *      tags={"Pokemon"},
     *      security={
     *          {"passport": {}},
     *      },
     *      summary="Show a Pokemon",
     *      description="Show a Pokemon",
     *      @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *      ),
     *          
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *           response=400,
     *           description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="not found"
     *      ),
     *  )
     */
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pokemon  $pokemon
     * @return \Illuminate\Http\Response
     */
    public function show(Pokemon $pokemon)
    {
        return new PokemonResource($pokemon);
    }

    /**
     * @OA\Put(
     *      path="/api/pokemon/{id}",
     *      operationId="postPokemonUpdate",
     *      tags={"Pokemon"},
     *      security={
     *          {"passport": {}},
     *      },
     *      summary="Update a Pokemon",
     *      description="Update a Pokemon",
     *      @OA\Parameter(
     *         name="name",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="hp",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="isFirstEdition",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="boolean"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="expansion",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="type",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="rarity",
     *         in="query",
     *         required=true,
     *         description= "Only Accept: Comun, No Comun or Rara",
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="price",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="imageUrl",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *          
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *           response=400,
     *           description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="not found"
     *      ),
     *  )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pokemon  $pokemon
     * @return \Illuminate\Http\Response
     */
    public function update(PokemonUpdateRequest $request, Pokemon $pokemon)
    {
        $pokemon->update($request->validated());

        return new PokemonResource($pokemon);
    }

    /**
     * @OA\Delete(
     *      path="/api/pokemon/{id}",
     *      operationId="postPokemonDelete",
     *      tags={"Pokemon"},
     *      security={
     *          {"passport": {}},
     *      },
     *      summary="Delete a Pokemon",
     *      description="Delete a Pokemon",
     *      @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *      ),
     *          
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *           response=400,
     *           description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="not found"
     *      ),
     *  )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pokemon  $pokemon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pokemon $pokemon)
    {
        $pokemon->delete();

        return response([
            'message' => 'The card of pokemon ' . $pokemon->name . ' was deleted'
        ], 200);
    }
}
