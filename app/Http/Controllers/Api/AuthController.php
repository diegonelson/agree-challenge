<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use Hash;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *      path="/api/login",
     *      summary="Login",
     *      tags={"Authentication"},
     *      @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *      ),
     *      @OA\Response(
     *         response=200,
     *         description="Authenticated",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error."
     *     )
     * )
     */
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(UserLoginRequest $request)
    {
        if (!Auth::attempt($request->validated())) {
            return response()->json(['error' => 'Unauthorised'], 401);
        } else {
            return response([
                'token' => Auth::user()->createToken('authToken')->accessToken,
                'user' => Auth::user()
            ]);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/register",
     *      summary="Register",
     *      tags={"Authentication"},
     *      @OA\Parameter(
     *         name="name",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="password_confirmation",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *      ),
     *      @OA\Response(
     *         response=200,
     *         description="User Registered and Authenticated",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error."
     *     ),
     *     
     * )
     */
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(UserRegisterRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return response([
            'status' => 'success',
            'user' => $user
        ]);
    }
}
