<?php

namespace Tests\Unit\Controller\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function store_a_user()
    {
        $response = $this->json('POST','/api/register',[
            "name"                  => "Juan Perez",
            "email"                 => "juan@perez.com",
            "password"              => "password123",
            "password_confirmation" => "password123"
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('user.email', 'juan@perez.com');
    }
}
